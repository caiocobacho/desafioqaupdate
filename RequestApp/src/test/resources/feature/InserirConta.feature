#language:pt
Feature: Cadastro de contas

Como um usu�rio 
Gostaria de cadastrar contas
Para que eu possa distribuir meu dinheiro de uma forma mais organizada

Scenario: Deve inserir uma conta com sucesso
Given que estou acessando a aplica��o
When informo o usu�rio "admin@phptravels.com"
And a senha "demoadmin"
And seleciono entrar
Then visualizo a p�gina inicial
When seleciono Contas
And seleciono Adicionar
And informo a conta "Conta de Teste"
And seleciono Salvar
Then a conta � inserida com sucesso

@ignore
Scenario: N�o deve inserir uma conta sem nome
Given que estou acessando a aplica��o
When informo o usu�rio "a@a"
And a senha "a"
And seleciono entrar
Then visualizo a p�gina inicial
When seleciono Contas
And seleciono Adicionar
And seleciono Salvar
Then sou notificar que o nome da conta � obrigat�rio
@ignore
Scenario: N�o deve inserir uma conta com nome j� existente 
Given que estou acessando a aplica��o
When informo o usu�rio "a@a"
And a senha "a"
And seleciono entrar
And visualizo a p�gina inicial
When seleciono Contas
And seleciono Adicionar
And informo a conta "Conta de Teste"
And seleciono Salvar
Then sou notificado que j� existe uma conta com esse nome