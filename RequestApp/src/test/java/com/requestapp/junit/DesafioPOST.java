package com.requestapp.junit;

import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import net.serenitybdd.junit.runners.SerenityRunner;


@RunWith(SerenityRunner.class)

public class DesafioPOST {
	@BeforeClass
	public static void init()
	{ 
	  RestAssured.baseURI="https://reqres.in";
	}

	@Test
public void POST() 
	{
	
		RequestSpecification request=RestAssured.given();

		request.header("Content-Type", "application/json");
		JSONObject json=new JSONObject();
		json.put("id","25");
		json.put("createdAt:",  "\2040-09-10\"");

		request.body(json.toJSONString());

		Response response=request.post("/api/users");

		int code=response.getStatusCode();

		Assert.assertEquals(code,201);



	}
	}