package com.requestapp.junit;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import net.serenitybdd.junit.runners.SerenityRunner;
import io.restassured.RestAssured;

 
@RunWith(SerenityRunner.class)  

public class DesafioGET {
	@BeforeClass
	public static void init()
	{ 
	  RestAssured.baseURI="https://jsonplaceholder.typicode.com";
	}
	
	@Test
	   public void getAllRequests() 
		{
			RestAssured.given()
			
			.when()
			.queryParam("completed", "true")
			.get("/todos")
			
			.then()
			.log()
			.all()
			.statusCode(200);
			
	    }
}


