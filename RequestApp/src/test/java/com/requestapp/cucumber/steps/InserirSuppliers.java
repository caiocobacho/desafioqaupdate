package com.requestapp.cucumber.steps;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.PendingException;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Ent�o;
import cucumber.api.java.pt.Quando;

public class InserirSuppliers {
	
	private WebDriver driver;
	
	@Given("^que desejo adicionar uma conta$")
	public void queDesejoAdicionarUmaConta() throws Throwable {
		driver = new ChromeDriver();
		driver.get("https://www.phptravels.net/admin");
		driver.findElement(By.id("Email")).sendKeys("admin@phptravels.com");
		driver.findElement(By.name("Password")).sendKeys("demoadmin");
		driver.findElement(By.tagName("button")).click();
		driver.findElement(By.linkText("Accounts")).click();
		driver.findElement(By.linkText("Suppliers")).click();
		driver.findElement(By.linkText("Add")).click();
	}

	@When("^adiciono a conta \"([^\"]*)\"$")
	public void addsuplliers(String arg1) throws Throwable {
		driver.findElement(By.id("First Name")).sendKeys("Caio");
		driver.findElement(By.id("Last Name")).sendKeys("Cobacho");
		driver.findElement(By.id("Password")).sendKeys("atypical");
		driver.findElement(By.id("Email Adress")).sendKeys("teste@hotmail.com");
		driver.findElement(By.id("Mobile Number")).sendKeys("123564545");
		driver.findElement(By.id("adress1")).sendKeys("random");
		driver.findElement(By.id("adress2")).sendKeys("random2");
		driver.findElement(By.id("itemname")).sendKeys("bemlocoempolganteH");
		driver.findElement(By.id("Country")).click();
		driver.findElement(By.id("Albania")).click();
		
		driver.findElement(By.tagName("Submit")).click();
	}



	@Then("^recebo a mensagem \"([^\"]*)\"$")
	public void receboAMensagem(String arg1) throws Throwable {
		String texto = driver.findElement(By.xpath("//div[starts-with(@class, 'alert alert-')]")).getText();
	    Assert.assertEquals(arg1, texto);
	}
	
	
	public void screenshot(Scenario cenario) {
		File file = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(file, new File("target/screenshot/"+cenario.getId()+".jpg"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	

	public void fecharBrowser() {
		driver.quit();
		System.out.println("terminando");
	}
	
}
